@ Author: Brannon Brakefield
@ Date: March 2017
.text
.global main
.extern printf
main:
	@ display memory at address1 before the COPYDATA takes place
	@ do this for address 1
	LDR r0, =output_prompt1
	BL printf

	LDR r0, =address1	@ prepare the prompt to be displayed
	BL printf		@ display the prompt

	LDR r0, =newline
	BL printf

	LDR r0, =output_prompt2
	BL printf

	LDR r0, =address2
	BL printf

	LDR r0, = newline
	BL printf

	@ ask for length value
	LDR r0, =length_prompt
	BL printf

	LDR r0, =input_format
	LDR r1, =length
	BL scanf

	LDR r0, =newline
	BL printf

	LDR r0, =copy_prompt
	BL printf

	LDR r0, =input_format
	LDR r1, =choice
	BL scanf

	LDR r0,=newline
	BL printf

	@ move values into the correct registers.
	LDR r0, =address1
	LDR r1, =address2
	LDR r2, =length
	LDR r2, [r2]
	LDR r3, =choice
	LDR r3, [r3]
	MOV r5, #0		@ register which shall act as a counter
	BL copydata		@ copy data

zeroterm:
	LDR r0, =zero_error
	BL printf
	B break
badsource:
	LDR r0,=source_error
	BL printf
	B break
end:
break:
	LDR r0, =newline
	BL printf

	MOV r7, #1
	SVC 0

copydata:
	CMP r2, #0
	BLE zeroterm
	CMP r3, #1
	BEQ copyadd1
	CMP r3, #2
	BNE badsource
	B copyadd2

copyadd1:
	ADD r5, r5, #1		@ increment counter
	LDRB r4, [r0], #1	@ load data from address 1 into buffer register
	STRB r4, [r1], #1	@ store data from buffer register into address 2
	CMP  r4, #0		@ if we haven't reached the end
	CMPNE r5, r2		@ and we havent reached our length limit
	BNE copyadd1		@ keep copying

	@ when done load the string into a register and print
	LDR r0, =output_prompt2
	MOV r1, #0
	BL printf
        LDR r0, =address2
	BL printf
	B end		@ break from subroutine when finished

copyadd2:
	ADD r5, r5, #1
	LDRB r4, [r1], #1	@ load data from address 2 into buffer register
	STRB r4, [r0], #1	@ store data from buffer register into address 1
	CMP r4, #0		@ if we haven't reached the end
	CMPNE r5, r2		@ and we haven't reached our length limit
	BNE copyadd2		@ keep copying

	@ when done load the string into a register and print
        LDR r0, =output_prompt1
	MOV r1, #0
	BL printf
	LDR r0, =address1
	BL printf
	B end		@ break from subroutine when finished

@------------------------------------
@ Start data
.data

@ use null terminated strings
address1: .asciz "foobar\n"
address2: .asciz "brannon\n"
newline: .asciz "\n"
input_format: .asciz "%d"
output_format: .asciz "%s\n"
length_prompt: .asciz "Please enter a length value\n"
length: .word 0
choice: .word 0
length_format: .asciz "%d"
output_prompt1: .asciz "The current value stored in address 1 is:\n"
output_prompt2: .asciz "The current value stored in address 2 is:\n"
copy_prompt: .asciz "Which address would you like to use as a source? 1 or 2?\n"
zero_error: .asciz "Number of bytes to copy is 0 or lesser. Terminating\n"
source_error: .asciz "Sorry, that is not an address you can copy from."
